# Teste prático para desenvolvedor backend InfoTV
-----------------------------------------------

*   Este teste tem como objetivo avaliar a escrita do código, semântica, utilização de bons princípios, organização, clean code e arquitetura.
    
*   A aplicação deverá ser escrita utilizando a framework Laravel 5.8 ou 6.\*
    

## Briefing

*   O teste irá simular uma **API RESTful** para o gerenciamento de uma biblioteca de filmes.
    
*   O usuário poderá fazer um cadastro pessoal, conectar-se na plataforma utilizando o e-mail e a senha fornecidos no cadastro, cadastrar um filme com upload e atributos, listar os filmes, editar os dados de um filme e excluir um filme.
    

## Descrição dos pontos do sistema

1.  Cadastro de usuário
    
    *   Campos \[Nome / email / senha / confirmação de senha\]
        
2.  Autenticação
    
    *   Campos \[e-mail / senha\]
        
    *   Características: A autenticação deverá retornar um token JWT para ser usado na autenticação dos demais endpoints.
        
3.  Cadastro de filmes
    
    *   Campos \[Nome / arquivo / tamanho do arquivo / data de cadastro\]
        
    *   Características: O arquivo precisa ser validado quanto o seu formato (exclusivamente vídeos) e tamanho máximo de 5mb.
        
4.  Cadastro de TAGS para o filme
    
    *   Campos \[Nome da tag\]
        
    *   Características: A tabela de tags deverá ter uma chave estrangeira para o filme, um filme pode ter N tags.
        
5.  Listagem de filmes
    
    *   Características: O usuário poderá listar os filmes com ordenação ascendente por nome, e com a opção de enviar por parâmetro GET outras opções de ordenação \[ASC / DESC\]
        
6.  Edição de filmes
    
    *   Características: O usuário poderá alterar o nome de um filme, remover tags ou adicionar novas tags.
        
7.  Exclusão de filmes
    
    *   Características: O usuário poderá excluir um filme, e ao excluir, as tags deverão ser excluídas.
        

## O que nós esperamos do seu teste

*   Resolução de problemas utilizando os recursos da framework
    
*   Banco de dados relacional (MySQL ou PostgreSQL)
    
*   Utilização de migrations e seeds
    

## O que nós ficaríamos felizes de ver em seu teste

*   Testes unitários
    
*   Utilização do POSTMAN para testes de interface, caso utilize, nos envie a coleção em JSON.
    

## O que nos impressionaria

*   Front-end utilizando VUE.JS e consumindo a API para consulta.
    
*   Documentação
    

## O que nós não gostaríamos

*   Descobrir que não foi você quem fez seu teste
    
*   Ver commits gigantes, sem mensagens ou com -m sem pé nem cabeça
    
*   Utilização do scaffold Auth do Laravel, queremos todo o fluxo de cadastro e autenticação feitos por ti!
    

## O que avaliaremos de seu teste

*   Histórico de commits do git
    
*   As instruções de como rodar o projeto
    
*   Organização, semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões
    
*   Alcance dos objetivos propostos
    

## Como entrego meu teste?

*   Nos envie um email com o link do seu repositório para [carlos.eduardo@infotv.com.br](mailto:carlos.eduardo@infotv.com.br "mailto:carlos.eduardo@infotv.com.br")![](mailspring://link-tracking/assets/ic-tracking-unvisited@2x.png "This link has not been clicked (mailto:carlos.eduardo@infotv.com.br)")